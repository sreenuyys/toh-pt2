import { Hero } from './hero';

export const HEROES: Hero[] = [
  { id: 11, name: 'Dr' },
  { id: 12, name: 'Nadgdg' },
  { id: 13, name: 'Bombasto' },
  { id: 14, name: 'Celeritas' },
  { id: 15, name: 'Magneta' },
  { id: 16, name: 'RubberMan' },
  { id: 17, name: 'Dynama' },
  { id: 18, name: 'Drff' },
  { id: 19, name: 'rambabu' },
  { id: 20, name: 'siva' }
];
